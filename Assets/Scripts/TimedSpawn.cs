﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Cave;
using System;

public class TimedSpawn : MonoBehaviour {

    public GameObject spawnee;
    public Text countText;
    public bool areSpawning = true;
    public float spawnDelay;
    [Tooltip("Maximum number of targets to spawn. -1 for infinitely many")]
    public long maxNumber = -1;
    public float ballSpeed = 200f;
    public List<Vector3> directions = new List<Vector3> { 
        new Vector3(0, 0, 1), 
        new Vector3(0, 1, 0),
        new Vector3(0, 1, 1),
        new Vector3(1, 0, 0),
        new Vector3(1, 0, 1),
        new Vector3(1, 1, 0),
        new Vector3(1, 1, 1),
    };

    private float sinceLastSpawn;
    private int count = 0;
    private int currDir = 0;
    private const string baseText = "Ball count: ";

	// Use this for initialization
	void Start () {
        setCountText();
    }

    void Update () {
        sinceLastSpawn += TimeSynchronizer.deltaTime;
        if(count < maxNumber && areSpawning){
            if(sinceLastSpawn >= spawnDelay){
				SpawnObject();
            }
        }
    }

    private void setCountText(){
        countText.text = baseText + count.ToString();
    }

    private void SpawnObject(){
        sinceLastSpawn = 0f;
        var obj = Instantiate(spawnee, transform.position, transform.rotation);
        obj.transform.parent = transform;
        obj.GetComponent<Rigidbody>().velocity = getDirection() * ballSpeed;
        NetworkServer.Spawn(obj);
        count++;
        setCountText();
    }

    private Vector3 getDirection()
    {
        currDir++;
        return directions[currDir % directions.Count];
    }
}
